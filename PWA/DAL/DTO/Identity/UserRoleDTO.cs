using System;

namespace PWA.DAL.DTO.Identity
{
    public class UserRoleDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsBelong { get; set; }
    }
}
