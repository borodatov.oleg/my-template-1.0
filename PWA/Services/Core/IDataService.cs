﻿namespace PWA.Services.Core
{
    using System;
    using DAL;

    public interface IDataService
    {
        void ConfigureDataContext(Action<IDataContext> action);
    }
}