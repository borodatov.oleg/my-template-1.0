namespace PWA.DAL.DTO.Groups
{
    using Identity;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Data transfer object for groups
    /// </summary>
    public class GroupDTO
    {
        /// <summary>
        /// Entity identity field
        /// </summary>
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int CompanyId { get; set; }
        public List<UserDTO> Users { get; set; }
        public bool Owned { get; set; }
        public string UserId { get; set; }
    }
}