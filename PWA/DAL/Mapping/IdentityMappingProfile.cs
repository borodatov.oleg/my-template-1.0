using AutoMapper;
using PWA.DAL.DTO.Groups;
using PWA.DAL.DTO.Identity;
using PWA.DAL.Models.Enums;
using PWA.DAL.Models.Groups;
using PWA.DAL.Models.Identity;
using System.Linq;

namespace BBWM.Core.Membership.Mapping
{
    public class IdentityMappingProfile : Profile
    {
        public IdentityMappingProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(d => d.Roles, m => m.MapFrom(s => s.UserRoles))
                .ForMember(d => d.Claims, m => m.Ignore())
                .ForMember(d => d.IsSuperAdmin, m => m.MapFrom(s => s.UserRoles.Where(x => x.Role.Type == RoleTypeEnum.SuperAdmin).Count() > 0))
                .ForMember(d => d.Groups, m => m.MapFrom(s => s.UserGroups))
                .ReverseMap();

            CreateMap<UserRole, UserRoleDTO>()
                 .ForMember(d => d.Name, m => m.MapFrom(s => s.Role.Name))
                .ReverseMap();

            CreateMap<Role, RoleDTO>()
                .ReverseMap();

            CreateMap<Group, GroupDTO>()
                .ForMember(d => d.Id, m => m.MapFrom(s => s.Id))
                .ForMember(d => d.Name, m => m.MapFrom(s => s.Name))
                .ReverseMap();

            CreateMap<UserGroup, GroupDTO>()
                .ForMember(d => d.Id, m => m.MapFrom(s => s.Group.Id))
                .ForMember(d => d.Name, m => m.MapFrom(s => s.Group.Name))
                .ReverseMap();
        }
    }
}