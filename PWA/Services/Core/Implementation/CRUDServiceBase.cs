﻿using System.Threading;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PWA.DAL;
using PWA.DAL.Core;

namespace PWA.Services.Core.Implementation
{
    public abstract class CRUDServiceBase<TEntityReadDto, TEntityEditDto, TKey>: IReadService<TEntityReadDto, TKey>, IEditService<TEntityEditDto, TKey>
        where TEntityReadDto : EntityDTO
        where TEntityEditDto : EntityDTO
    {
        protected readonly IReadService<TEntityReadDto, TKey> ReadService;
        protected readonly IEditService<TEntityEditDto, TKey> EditService;

        protected CRUDServiceBase(IReadService<TEntityReadDto, TKey> readService, IEditService<TEntityEditDto, TKey> editService)
        {
            ReadService = readService;
            EditService = editService;
        }

        public Task<TEntityReadDto> GetAsync(TKey id, CancellationToken token)
        {
            return ReadService.GetAsync(id, token);
        }

        public Task<List<TEntityReadDto>> GetAllAsync(Guid userId, CancellationToken token)
        {
            return ReadService.GetAllAsync(userId, token);
        }

        public Task<TKey> SaveAsync(TEntityEditDto item, CancellationToken token)
        {
            return EditService.SaveAsync(item, token);
        }

        public Task<bool> DeleteAsync(TKey id, CancellationToken token)
        {
            return EditService.DeleteAsync(id, token);
        }

        public Task<bool> DeleteAllAsync(CancellationToken token)
        {
            return EditService.DeleteAllAsync(token);
        }

        public void ConfigureDataContext(Action<IDataContext> action)
        {
            EditService.ConfigureDataContext(action);
            ReadService.ConfigureDataContext(action);
        }
    }


    public abstract class CRUDServiceBase<TEntityReadDto, TEntityEditDto> : CRUDServiceBase<TEntityReadDto, TEntityEditDto, Guid>,
            IReadService<TEntityReadDto>, IEditService<TEntityEditDto>
        where TEntityReadDto : EntityDTO
        where TEntityEditDto : EntityDTO
    {
        protected CRUDServiceBase(IReadService<TEntityReadDto> readService, IEditService<TEntityEditDto> editService) : base(readService, editService)
        {
        }
    }

    public abstract class CRUDServiceBase<TEntityDto> : CRUDServiceBase<TEntityDto, TEntityDto>
        where TEntityDto : EntityDTO
    {
        protected CRUDServiceBase(IReadService<TEntityDto> readService, IEditService<TEntityDto> editService) : base(readService, editService)
        {
        }
    }
}