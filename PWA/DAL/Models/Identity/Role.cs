using Microsoft.AspNetCore.Identity;
using PWA.DAL.Models.Enums;
using System;
using System.Collections.Generic;

namespace PWA.DAL.Models.Identity
{
    public class Role : IdentityRole<Guid>
    {  
        public Role() : base() { }

        public Role(string roleName) : base(roleName) { }

        public virtual ICollection<UserRole> UserRoles { get; } = new List<UserRole>();

        public RoleTypeEnum Type;
    }
}
