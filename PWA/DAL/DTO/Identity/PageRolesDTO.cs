﻿using System.Collections.Generic;

namespace PWA.DAL.DTO.Identity
{
    public class PageRolesDTO
    {
        public string Path { get; set; }

        public string Title { get; set; }

        public List<string> Roles { get; set; }

        public Dictionary<string, string> Claims { get; set; }
    }
}