import { Routes } from "@angular/router";
import { CategoryComponent } from "./categories/category.component";
import { LoginComponent } from "./login/login.component";
import { SignupComponent } from "./signup/signup.component";
import { AdminComponent } from "./admin/admin.component";

export const routesConfig: Routes = [
  {
    path: "category",
    component: CategoryComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "signup",
    component: SignupComponent
  },
  {
    path: "admin",
    component: AdminComponent,
    canActivate: ["adminsOnlyGuard"]
  },
  {
    path: "",
    redirectTo: "/category",
    pathMatch: "full"
  },
  {
    path: "**",
    redirectTo: "/category",
    pathMatch: "full"
  }
];
