namespace PWA.DAL.DTO.Identity
{
    public class RoleDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool CheckIp { get; set; }
    }
}