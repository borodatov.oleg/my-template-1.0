﻿namespace PWA.Services.Core.Implementation
{
    using System;
    using DAL;

    public abstract class DataService : IDataService
    {
        protected readonly IDataContext _context;

        protected DataService(IDataContext context)
        {
            _context = context;
        }

        public void ConfigureDataContext(Action<IDataContext> action)
        {
            action?.Invoke(_context);
        }
    }
}