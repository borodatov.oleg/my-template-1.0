import { shareReplay, filter, tap, map } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, BehaviorSubject } from "rxjs";
import { User } from "../model/user";

export const ANONYMOUS_USER: User = {
  id: "00000000-0000-0000-0000-000000000000",
  email: undefined,
  roles: []
};

@Injectable()
export class AuthService {
  private subject = new BehaviorSubject<User>(undefined);

  user$: Observable<User> = this.subject
    .asObservable()
    .pipe(filter(user => !!user));

  isLoggedIn$: Observable<boolean> = this.user$.pipe(map(user => user.id !== "00000000-0000-0000-0000-000000000000"));

  isLoggedOut$: Observable<boolean> = this.isLoggedIn$.pipe(
    map(isLoggedIn => !isLoggedIn)
  );

  constructor(private http: HttpClient) {
    http
      .get<User>("/api/account/user")
      .subscribe(user => this.subject.next(user ? user : ANONYMOUS_USER));
  }

  signUp(email: string, password: string) {
    return this.http
      .post<User>("/api/account/register", { email, password })
      .pipe(
        shareReplay(),
        tap(user => this.subject.next(user))
      );
  }

  login(email: string, password: string) {
    return this.http
      .post<User>("/api/account/login", { email, password })
      .pipe(
        shareReplay(),
        tap(user => this.subject.next(user))
      );
  }

  loginAsUser(email: string) {
    return this.http
      .post<User>("/api/admin", { email })
      .pipe(
        shareReplay(),
        tap(user => this.subject.next(user))
      );
  }

  logout(): Observable<any> {
    return this.http.get("/api/account/logout").pipe(
      shareReplay(),
      tap(user => this.subject.next(ANONYMOUS_USER))
    );
  }
}
