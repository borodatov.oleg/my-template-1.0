﻿using System.Collections.Generic;

namespace PWA.DAL.DTO.Identity
{
    /// <summary>
    /// Permissions to access controller action
    /// </summary>
    public class RouteRolesDTO
    {
        /// <summary>
        /// Action path
        /// </summary>
        public string ActionPath { get; set; }

        /// <summary>
        /// HTTP Method
        /// </summary>
        public string ActionVerb { get; set; }

        /// <summary>
        /// Roles
        /// </summary>
        public List<string> Roles { get; set; } = new List<string>();
    }
}