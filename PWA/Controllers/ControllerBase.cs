namespace PWA.Controllers
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    public abstract class ControllerBase : Controller
    {
        protected readonly ILogger<ControllerBase> _logger;
        protected readonly IHostingEnvironment _hostingEnvironment;

        protected ControllerBase(ILogger<ControllerBase> logger, IHostingEnvironment hostingEnvironment)
        {
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
        }

        protected IActionResult HandleRequest(Func<IActionResult> request)
        {
            if (ModelIsValid)
            {
                try
                {
                    return request();
                }
                catch (Exception ex)
                {
                    return HandleExceptionAsync(ex).Result;
                }
            }

            return BadRequest(ModelState);
        }

        protected async Task<IActionResult> HandleRequestAsync(Func<Task<IActionResult>> request)
        {
            if (ModelIsValid)
            {
                try
                {
                    return await request();
                }
                catch (Exception ex)
                {
                    return await HandleExceptionAsync(ex);
                }
            }

            return BadRequest(ModelState);
        }
        private bool ModelIsValid => ModelState.IsValid || ModelState.ErrorCount == 0;

        protected IActionResult NotImplemented(string message = null)
        {
            return StatusCode(StatusCodes.Status501NotImplemented, message);
        }

        protected IActionResult InternalServerError(object message)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, message);
        }

        protected string CalcUrl(string tail)
        {
            return Request.Scheme + "://" + Request.Host + "/" + tail;
        }

        private async Task<IActionResult> HandleExceptionAsync(Exception ex)
        {
            var description = "Internal error";
            var isDevelopment = _hostingEnvironment.IsDevelopment();
            if (isDevelopment)
            {
                description = ex.ToString();
            }

            var msgObj = new { Message = description, Id = Guid.NewGuid() };

            _logger.LogError(ex, "Internal Server Error (Id: {ErrorId})", msgObj.Id.ToString());

            return InternalServerError(msgObj);
        }
    }
}