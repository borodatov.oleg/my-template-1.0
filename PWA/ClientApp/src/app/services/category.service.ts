import { map } from "rxjs/operators";

import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Category } from "../model/category";
import { Observable } from "rxjs";

@Injectable()
export class CategoryService {
  constructor(private http: HttpClient) {}

  loadAll(): Observable<Category[]> {
    return this.http.get<any>("/api/categories/all").pipe(map(res => res.categories));
  }

  findById(id: string) {
    return this.http.get<Category>("/api/categories/" + id);
  }
}
