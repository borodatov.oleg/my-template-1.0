using System.Collections.Generic;
using System;

namespace PWA.DAL.DTO.Identity
{
    public class UserLoginDTO
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public IList<string> Roles { get; set; }
    }
}