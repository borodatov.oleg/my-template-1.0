﻿namespace PWA.Services.Classes
{
    using AutoMapper;
    using DAL.DTO;
    using DAL.Models;
    using Core.Implementation;
    using Interfaces;
    using DAL;

    public class CategoryReadService : ReadServiceBase<Category, CategoryDto>, ICategoryReadService
    {
        public CategoryReadService(IDataContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }

    public class CategoryEditService : EditServiceBase<Category, CategoryDto>, ICategoryEditService
    {
        public CategoryEditService(IDataContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }

    public class CategoryService : CRUDServiceBase<CategoryDto>, ICategoryService
    {
        public CategoryService(ICategoryReadService readService, ICategoryEditService editService) : base(readService, editService)
        {
        }
    }
}
