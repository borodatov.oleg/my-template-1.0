using System.Threading;
namespace PWA.Services.Core.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using AutoMapper;
    using DAL;
    using PWA.DAL.Core;

    public abstract class ReadServiceBase<TEntity, TEntityDto, TKey> : DataService, IReadService<TEntityDto, TKey>
        where TEntity : Entity
        where TEntityDto : EntityDTO
    {
        protected readonly IMapper Mapper;

        protected ReadServiceBase(IDataContext context, IMapper mapper) : base(context)
        {
            Mapper = mapper;
        }

        public virtual async Task<TEntityDto> GetAsync(TKey id, CancellationToken token)
        {
            var entity = await _context.Set<TEntity>().FindAsync(id, token);
            return Mapper.Map<TEntityDto>(entity);
        }

        public virtual async Task<List<TEntityDto>> GetAllAsync(Guid userId, CancellationToken token)
        {
            var items = await GetQueryable().Where(e => e.UserId == userId).ToListAsync(token);
            return Mapper.Map<List<TEntityDto>>(items);
        }

        protected virtual IQueryable<TEntity> GetQueryable()
        {
            return _context.Set<TEntity>();
        }
    }

    public abstract class ReadServiceBase<TEntity, TEntityDto> : ReadServiceBase<TEntity, TEntityDto, Guid>, IReadService<TEntityDto>
        where TEntity : Entity
        where TEntityDto : EntityDTO
    {
        protected ReadServiceBase(IDataContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}