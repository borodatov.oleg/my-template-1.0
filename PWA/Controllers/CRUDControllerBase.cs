﻿namespace PWA.Controllers
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using DAL.DTO;
    using Services.Interfaces;
    using Microsoft.AspNetCore.Identity;
    using PWA.DAL.Models.Identity;
    using PWA.Services.Core;
    using PWA.DAL.Core;

    public abstract class CRUDControllerBase<TEntityDTO, TKey> : ControllerBase
        where TEntityDTO : EntityDTO
    {
        protected IReadService<TEntityDTO, TKey> _readService;
        protected IEditService<TEntityDTO, TKey> _editService;
        private readonly UserManager<User> _userManager;

        protected CRUDControllerBase(
            IReadService<TEntityDTO, TKey> readService,
            IEditService<TEntityDTO, TKey> editService,
            ILogger<CRUDControllerBase<TEntityDTO, TKey>> logger,
            IHostingEnvironment hostingEnvironment,
            UserManager<User> userManager) : base(logger, hostingEnvironment)
        {
            _readService = readService;
            _editService = editService;
            _userManager = userManager;
        }

        protected CRUDControllerBase(
            IReadService<TEntityDTO, TKey> readService,
            ILogger<CRUDControllerBase<TEntityDTO, TKey>> logger,
            IHostingEnvironment hostingEnvironment,
            UserManager<User> userManager) : this(readService, null, logger, hostingEnvironment, userManager)
        {
        }

        protected CRUDControllerBase(
            IEditService<TEntityDTO, TKey> editService,
            ILogger<CRUDControllerBase<TEntityDTO, TKey>> logger,
            IHostingEnvironment hostingEnvironment,
            UserManager<User> userManager) : this(null, editService, logger, hostingEnvironment, userManager)
        {
        }

        [HttpGet, Route("{id}")]
        public virtual Task<IActionResult> Get(TKey id, CancellationToken cancellationToken = default)
        {
            return HandleReadRequestAsync(async () =>
            {
                var res = await _readService.GetAsync(id, cancellationToken);
                if (res == null)
                {
                    return NotFound();
                }
                return Ok(res);
            });
        }

        [HttpGet, Route("all")]
        public virtual Task<IActionResult> GetAll(CancellationToken cancellationToken = default)
        {
            return HandleReadRequestAsync(async () => Ok(await _readService.GetAllAsync(GetLoggedUserId(), cancellationToken)));
        }

        [HttpPost]
        public virtual Task<IActionResult> Post([FromBody]TEntityDTO dto, CancellationToken cancellationToken = default)
        {
            dto.UserId = GetLoggedUserId();
            return HandleEditRequestAsync(async () => Ok(await _editService.SaveAsync(dto, cancellationToken)));
        }

        [HttpPost, Route("{id}")]
        public virtual Task<IActionResult> Put(TKey id, [FromBody]TEntityDTO dto, CancellationToken cancellationToken = default)
        {
            dto.UserId = GetLoggedUserId();
            return HandleEditRequestAsync(async () => Ok(await _editService.SaveAsync(dto, cancellationToken)));
        }

        [HttpGet, Route("Delete/{id}")]
        public virtual Task<IActionResult> Delete(TKey id, CancellationToken cancellationToken = default)
        {
            return HandleEditRequestAsync(async () =>
            {
                if (!await _editService.DeleteAsync(id, cancellationToken))
                {
                    return NotFound(id);
                }
                return Ok(true);
            });
        }

        [HttpGet, Route("Delete/all")]
        public virtual Task<IActionResult> DeleteAll(CancellationToken cancellationToken = default)
        {
            return HandleEditRequestAsync(async () =>
            {
                if (await _editService.DeleteAllAsync(cancellationToken))
                {
                    return Ok();
                }
                return BadRequest("You can't delete all");
            });
        }

        protected async Task<IActionResult> HandleReadRequestAsync(Func<Task<IActionResult>> request)
        {
            if (_readService == null)
            {
                return NotImplemented();
            }
            return await HandleRequestAsync(request);
        }

        protected async Task<IActionResult> HandleEditRequestAsync(Func<Task<IActionResult>> request)
        {
            if (_editService == null)
            {
                return NotImplemented();
            }
            return await HandleRequestAsync(request);
        }

        protected Guid GetLoggedUserId()
        {
            var id = _userManager.GetUserId(User);
            return Guid.Parse(id);
        }
    }

    public abstract class CRUDControllerBase<TEntityDTO> : CRUDControllerBase<TEntityDTO, Guid>
        where TEntityDTO : EntityDTO
    {
        protected CRUDControllerBase(
            IReadService<TEntityDTO, Guid> readService,
            IEditService<TEntityDTO, Guid> editService,
            ILogger<CRUDControllerBase<TEntityDTO>> logger,
            IHostingEnvironment hostingEnvironment,
            UserManager<User> userManager) : base(readService, editService, logger, hostingEnvironment, userManager)
        {
        }

        protected CRUDControllerBase(
            IReadService<TEntityDTO, Guid> readService,
            ILogger<CRUDControllerBase<TEntityDTO>> logger,
            IHostingEnvironment hostingEnvironment,
            UserManager<User> userManager) : base(readService, logger, hostingEnvironment, userManager)
        {
        }

        protected CRUDControllerBase(
            IEditService<TEntityDTO, Guid> editService,
            ILogger<CRUDControllerBase<TEntityDTO>> logger,
            IHostingEnvironment hostingEnvironment,
            UserManager<User> userManager) : base(editService, logger, hostingEnvironment, userManager)
        {
        }
    }
}