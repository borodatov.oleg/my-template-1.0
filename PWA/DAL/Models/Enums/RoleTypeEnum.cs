using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PWA.DAL.Models.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RoleTypeEnum
    {
        [EnumMember(Value = "SuperAdmin")] SuperAdmin = 1,

        [EnumMember(Value = "Admin")] Admin,

        [EnumMember(Value = "User")] User
    }
}