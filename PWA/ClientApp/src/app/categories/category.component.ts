import { of as observableOf, Observable } from "rxjs";

import { catchError } from "rxjs/operators";
import { Component, OnInit } from "@angular/core";
import { CategoryService } from "../services/category.service";
import { Category } from "../model/category";
import { AuthService } from "../services/auth.service";

@Component({
  selector: "category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.css"]
})
export class CategoryComponent implements OnInit {
  category$: Observable<Category[]>;
  isLoggedIn$: Observable<boolean>;

  constructor(
    private categoryService: CategoryService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.category$ = this.categoryService
      .loadAll()
      .pipe(catchError(err => observableOf([])));
    this.isLoggedIn$ = this.authService.isLoggedIn$;
  }
}
