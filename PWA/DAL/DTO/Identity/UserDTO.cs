using PWA.DAL.DTO.Groups;
using Destructurama.Attributed;
using System.Collections.Generic;
using PWA.DAL.Models.Enums;
using System;
using PWA.DAL.Core;

namespace PWA.DAL.DTO.Identity
{
    public class UserDTO: EntityDTO
    {
        public UserDTO()
        {
            Roles = new List<UserRoleDTO>(); //HashSet<string>();
            Claims = new Dictionary<string, string>();
        }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName); } }
        [NotLogged] public string Password { get; set; }
        [NotLogged] public string ConfirmPassword { get; set; }
        public List<UserRoleDTO> Roles { get; set; }
        public Dictionary<string, string> Claims { get; set; }
        public bool? LockoutEnabled { get; set; }
        public string RecoveryCode { get; set; }
        public bool IsSuperAdmin { get; set; }
        public List<GroupDTO> Groups { get; set; }
        public AccountStatusEnum AccountStatus { get; set; }
    }
}