﻿namespace PWA.DAL.Core
{
    using System;

    /// <summary>
    /// Base class for all entities dto
    /// </summary>
    public abstract class EntityDTO
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
    }
}
