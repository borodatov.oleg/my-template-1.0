using PWA.DAL.Core;
using PWA.DAL.Models.Groups;
using System.ComponentModel.DataAnnotations.Schema;

namespace PWA.DAL.Models.Identity
{
    [Table("AspNetUserGroups")]
    public class UserGroup : Entity
    {
        public virtual User User { get; set; }

        public int GroupId { get; set; }
        public virtual Group Group { get; set; }
    }
}
