﻿namespace PWA.DAL.DTO
{
    using Core;

    public class CategoryDto : EntityDTO
    {
        public string Name { get; set; }
    }
}
