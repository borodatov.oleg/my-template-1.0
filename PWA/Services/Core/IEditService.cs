﻿using System.Threading;
using System;
using System.Threading.Tasks;
using PWA.DAL.Core;

namespace PWA.Services.Core
{
    public interface IEditService<in TEntityDto, TKey> : IDataService
        where TEntityDto : EntityDTO
    {
        Task<TKey> SaveAsync(TEntityDto item, CancellationToken token);
        Task<bool> DeleteAsync(TKey id, CancellationToken token);
        Task<bool> DeleteAllAsync(CancellationToken token);
    }

    public interface IEditService<in TEntityDto> : IEditService<TEntityDto, Guid>
        where TEntityDto : EntityDTO
    {
    }
}
