﻿using System.Threading;
using System;
using System.Threading.Tasks;
using AutoMapper;
using PWA.DAL;
using PWA.DAL.Core;
using System.Linq;

namespace PWA.Services.Core.Implementation
{
    public abstract class EditServiceBase<TEntity, TEntityDto, TKey> : DataService, IEditService<TEntityDto, TKey>
        where TEntity : Entity, new()
        where TEntityDto : EntityDTO
    {
        protected readonly IMapper _mapper;

        protected EditServiceBase(IDataContext context, IMapper mapper) : base(context)
        {
            _mapper = mapper;
        }

        public virtual async Task<TKey> SaveAsync(TEntityDto item, CancellationToken token)
        {
            var dbSet = _context.Set<TEntity>();
            var entity = _mapper.Map<TEntity>(item);
            var id = GetId(entity);
            if (Equals(id, default(TKey)))
            {
                dbSet.Add(entity);
            }
            else
            {
                if (dbSet.Any(e => e.Id == item.Id && e.UserId == item.UserId))
                    dbSet.Update(entity);
            }
            await _context.SaveChangesAsync(token);
            return GetId(entity);
        }

        public virtual async Task<bool> DeleteAsync(TKey id, CancellationToken token)
        {
            var dbSet = _context.Set<TEntity>();
            var entity = dbSet.Find(id);
            if (entity == null) return false;
            dbSet.Remove(entity);
            await _context.SaveChangesAsync(token);
            return true;

        }
        public virtual async Task<bool> DeleteAllAsync(CancellationToken token)
        {
            var entity = _context.Set<TEntity>();

            _context.Set<TEntity>().RemoveRange(entity);
                await _context.SaveChangesAsync(token);
                return true;
        }

        protected virtual TKey GetId(TEntity entity)
        {
            return (TKey)entity.GetType().GetProperty("Id").GetValue(entity);
        }

        protected virtual TEntity SetId(TEntity entity, TKey value)
        {
            entity.GetType().GetProperty("Id").SetValue(entity, value);
            return entity;
        }
    }

    public abstract class EditServiceBase<TEntity, TEntityDto> : EditServiceBase<TEntity, TEntityDto, Guid>, IEditService<TEntityDto>
        where TEntity : Entity, new()
        where TEntityDto : EntityDTO
    {
        protected EditServiceBase(IDataContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}