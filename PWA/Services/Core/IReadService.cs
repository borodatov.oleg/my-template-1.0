﻿using System.Threading;
namespace PWA.Services.Core
{
	using System;
	using System.Collections.Generic;
	using System.Threading.Tasks;
	using PWA.DAL.Core;

    public interface IReadService<TEntityDto, TKey> : IDataService
	    where TEntityDto : EntityDTO
	{
		Task<TEntityDto> GetAsync(TKey id, CancellationToken token);

		Task<List<TEntityDto>> GetAllAsync(Guid userId, CancellationToken token);
	}

	public interface IReadService<TEntityDto> : IReadService<TEntityDto, Guid>
		where TEntityDto : EntityDTO
	{
    }
}
