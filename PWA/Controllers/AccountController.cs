using System;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ClaimTypes = System.Security.Claims.ClaimTypes;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;
using PWA.DAL.DTO.Identity;
using PWA.DAL.Models.Identity;
using PWA.DAL.Models.Enums;
using PWA.Services;
using PWA.Services.Interfaces;
using PWA.DAL.Models;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace PWA.Controllers
{
    [Produces("application/json")]
    [Route("api/account")]
    [AllowAnonymous]
    public class AccountApiController : Microsoft.AspNetCore.Mvc.ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly SignInManager<User> _signInManager;
        //private readonly IUserService _userService;
        private readonly IHttpContextAccessor _contextAccessor;
        readonly ILogger<AccountApiController> _logger;
        private readonly UrlEncoder _urlEncoder;
        private const string Scheme = "https"; // hardcoded because Request.Scheme not reliable with load balanced architecture especially with Azure

        public AccountApiController(
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            SignInManager<User> signInManager,
            IHttpContextAccessor contextAccessor,
            ILogger<AccountApiController> logger,
            IHostingEnvironment hostingEnvironment,
            UrlEncoder urlEncoder) : base()
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _contextAccessor = contextAccessor;
            _logger = logger;
            _urlEncoder = urlEncoder;
            _signInManager = signInManager;
        }

        private string DomainUrl => $"{_contextAccessor.HttpContext.Request.Scheme}://{_contextAccessor.HttpContext.Request.Host.Value}";

        private async Task<string> GenerateEmailConfirmationTokenAsync(User user)
        {
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            return WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(token));
        }

        private async Task SendEmailConfirmation(User user, bool genToken = true,
            CancellationToken cancellationToken = default)
        {
            if (_contextAccessor.HttpContext == null) return;

            // Generate callbackUrl
            var code = genToken ? await GenerateEmailConfirmationTokenAsync(user) : Guid.NewGuid().ToString();
            var callbackUrl = $"Hello! <br> <br> To confirm email <a href='{DomainUrl}/auth/confirm?userId={user.Id}&code={code}'>follow the link</a>. <br><br> Thank you for choosing our service!";

            user.EmailConfirmationToken = code;
            await _userManager.UpdateAsync(user);

            // await _mailSender.SendEmailAsync("Confirm email", callbackUrl, to: user.Email);
        }

        [HttpPost]
        [Route("resetpassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordDTO resetPassword, CancellationToken cancellationToken)
        {
            if (resetPassword == null)
            {
                return BadRequest("resetPassword parameter is null");
            }

            try
            {
                // await _userService.ResetPassword(resetPassword, cancellationToken);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
            return Ok();
        }

        [HttpPost]
        [Route("confirm-email")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail([FromBody] ConfirmEmailDTO confirmEmail, CancellationToken cancellationToken)
        {
            if (confirmEmail == null)
                return BadRequest("ConfirmEmail parameter is null");

            var user = await _userManager.FindByIdAsync(confirmEmail.UserId);
            if (user == null)
            {
                return BadRequest("User doesn't exist.");
            }

            if (confirmEmail.Code != user.EmailConfirmationToken)
            {
                return BadRequest("Activation code is invalid. Please contact your system administrator.");
            }

            var code = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(confirmEmail.Code));
            var result = await _userManager.ConfirmEmailAsync(user, code);

            if (!result.Succeeded)
                return BadRequest(string.Join(Environment.NewLine, result.Errors.Select(x => x.Description).ToList()));

            user.EmailConfirmationToken = null;
            await _userManager.UpdateAsync(user);
            await SetUserStatus(user.Id.ToString(), AccountStatusEnum.Active, cancellationToken);

            return NoContent();
        }

        public async Task SetUserStatus(string userId, AccountStatusEnum status, CancellationToken cancellationToken = default)
        {
            var user = await _userManager.FindByIdAsync(userId);
            user.AccountStatus = status;
            await _userManager.UpdateAsync(user);
        }

        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] UserRegistrationDTO userRegistrationData, CancellationToken cancellationToken)
        {
            var user = new User
            {
                UserName = userRegistrationData.Email,
                FirstName = userRegistrationData.FirstName,
                LastName = userRegistrationData.LastName,
                Email = userRegistrationData.Email,
                AccountStatus = AccountStatusEnum.Unverified
            };

            var res = await _userManager.CreateAsync(user, userRegistrationData.Password);

            var UserRole = await _roleManager.FindByNameAsync(RoleTypeEnum.User.ToString());
            if (UserRole == null)
            {
                UserRole = new Role(RoleTypeEnum.User.ToString());
                await _roleManager.CreateAsync(UserRole);
            }

            await _userManager.AddToRoleAsync(user, RoleTypeEnum.User.ToString());

            if (!res.Succeeded)
            {
                return BadRequest(res.Errors.First().Description);
            }

            user = await _userManager.Users
                .FirstOrDefaultAsync(u => u.Email == userRegistrationData.Email, cancellationToken);
            var userDto = new UserLoginDTO()
            {
                Id = user.Id,
                Email = user.Email,
                Roles = await _userManager.GetRolesAsync(user)
            };

            _logger.LogInformation("Registered user: {}", userRegistrationData.Email);

            return Ok(user);
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginDTO userLoginData, CancellationToken cancellationToken)
        {
            var user = await _userManager.Users
                .FirstOrDefaultAsync(u => u.Email == userLoginData.Email, cancellationToken);

            var signInResult = await _signInManager.PasswordSignInAsync(userLoginData.Email, userLoginData.Password, false, false);

            if (signInResult != null)
            {
                if (signInResult.Succeeded)
                {
                    // if (user.AccountStatus == AccountStatusEnum.Unverified)
                    // {
                    //     return BadRequest("Необходимо подтвердить адрес электронной почты. Для подтверждения, пожалуйста, отправьте пустое письмо на info@mymoneydrain.com");
                    // }

                    if (user.AccountStatus == AccountStatusEnum.Deleted)
                    {
                        return BadRequest("You can't log in to this account.");
                    }

                    // var authSettings = new AuthSettingsDTO { UserId = user.Id };
                    // authSettings.token = GenerateToken(user.SecurityStamp);
                    // authSettings.emailConfirmed = user.AccountStatus == AccountStatusEnum.Active;
                    var userDto = new UserLoginDTO()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Roles = await _userManager.GetRolesAsync(user)
                    };
                    return Ok(userDto);
                }
                else
                {
                    if (user != null)
                    {
                        if (signInResult.IsLockedOut)
                        {
                            var reasonPhrase = user.AccountStatus == AccountStatusEnum.Suspended ? "Your account has been suspended" : "The user is locked out.";
                            return BadRequest(reasonPhrase);
                        }

                        await _userManager.AccessFailedAsync(user);

                        if (user.AccessFailedCount >= 20)
                        {
                            await _userManager.SetLockoutEnabledAsync(user, true);
                            await _userManager.SetLockoutEndDateAsync(user, DateTimeOffset.UtcNow.AddMinutes(30));
                            await _userManager.ResetAccessFailedCountAsync(user);
                            return BadRequest("Too many attempts to log in. The user is locked out for 30 minutes");
                        }
                    }
                    
                    return BadRequest("Некорректный логин или пароль.");
                }
            }

            return BadRequest("User service unavailable");
        }

        [HttpGet]
        [Route("token")]
        public IActionResult GetToken()
        {
            return Ok(GenerateToken(User.Identity.Name));
        }

        [HttpGet]
        [Route("user")]
        public async Task<IActionResult> GetUser(CancellationToken cancellationToken)
        {
            var userDto = new UserLoginDTO();
            if (User.Identity.IsAuthenticated)
            {
                var user = await _userManager.Users.FirstOrDefaultAsync(u => u.UserName == User.Identity.Name, cancellationToken);
                if (user != null)
                {
                    userDto.Id = user.Id;
                    userDto.Email = user.Email;
                    userDto.Roles = await _userManager.GetRolesAsync(user);
                }
            }
            return Ok(userDto);
        }

        [HttpGet]
        [Route("logout")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("Logout is successfully.");
            return Ok();
        }

        private JwtInfo GenerateToken(string username)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("MyPersonalSuperSecretKey@345");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, username)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return new JwtInfo
            {
                Token = tokenHandler.WriteToken(token),
                //Audience = "http://mymoneydrain.com"
                Audience = "http://localhost:5000"
            };
        }
    }
}