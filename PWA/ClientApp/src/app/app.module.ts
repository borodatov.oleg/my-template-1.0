import { BrowserModule } from "@angular/platform-browser";
import { NgModule, Provider } from "@angular/core";
import { HttpClientModule, HttpClientXsrfModule } from "@angular/common/http";
import { AuthModule } from "../modules/auth/auth.module";

import { AppComponent } from "./app.component";
import { CategoryComponent } from "./categories/category.component";
import { LoginComponent } from "./login/login.component";
import { SignupComponent } from "./signup/signup.component";
import { routesConfig } from "./routes.config";
import { CategoryService } from "./services/category.service";
import { ReactiveFormsModule } from "@angular/forms";

import { AuthService } from "./services/auth.service";
import { AdminComponent } from "./admin/admin.component";
import { Router, RouterModule } from "@angular/router";

import { RbacAllowDirective } from "./common/rbac-allow.directive";
import { AuthorizationGuard } from "./services/authorization.guard";

export function createAdminOnlyGuard(authService: AuthService, router: Router) {
  return new AuthorizationGuard(["Admin"], authService, router);
}

@NgModule({
  declarations: [
    AppComponent,
    CategoryComponent,
    LoginComponent,
    SignupComponent,
    AdminComponent,
    RbacAllowDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: "XSRF-TOKEN",
      headerName: "x-xsrf-token"
    }),
    RouterModule.forRoot(routesConfig),
    ReactiveFormsModule,
    AuthModule
  ],
  providers: [
    CategoryService,
    AuthService,
    {
      provide: "adminsOnlyGuard",
      useFactory: createAdminOnlyGuard,
      deps: [AuthService, Router]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
