using PWA.DAL.Core;
using PWA.DAL.Models.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PWA.DAL.Models.Groups
{
    /// <summary>
    /// Group entity
    /// </summary>
    [Table("Groups")]
    public class Group : Entity
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        public virtual ICollection<UserGroup> UserGroups { get; set; }
    }
}