﻿namespace PWA.DAL.Models
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Core;

    [Table("Category")]
    public class Category : Entity
    {
        public string Name { get; set; }
    }
}
