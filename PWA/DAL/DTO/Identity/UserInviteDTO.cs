﻿namespace PWA.DAL.DTO.Identity
{
    public class UserInviteDTO
    {
        public string Email { get; set; }

        public string Code { get; set; }

        public string Password { get; set; }
    }
}
