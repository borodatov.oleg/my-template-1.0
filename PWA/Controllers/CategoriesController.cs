﻿namespace PWA.Controllers
{
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Logging;
    using Microsoft.AspNetCore.Mvc;
    using DAL.DTO;
    using DAL.Models.Identity;
    using Services.Interfaces;

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : CRUDControllerBase<CategoryDto>
    {
        public CategoriesController(UserManager<User> userManager,
            ICategoryService service,
            ILogger<CategoriesController> logger,
            IHostingEnvironment hostingEnvironment) :
            base(service, service, logger, hostingEnvironment, userManager)
        {}
    }
}
