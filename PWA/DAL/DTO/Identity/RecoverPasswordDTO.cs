﻿namespace PWA.DAL.DTO.Identity
{
    public class RecoverPasswordDTO
    {
        public string Email { get; set; }
    }
}
