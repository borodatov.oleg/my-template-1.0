using System;
using PWA.DAL.Models;

namespace PWA.DAL.DTO.Identity
{
    public class AuthSettingsDTO
    {
        public Guid UserId { get; set; }
        public JwtInfo token { get; set; }
        public bool emailConfirmed { get; set; }
    }
}