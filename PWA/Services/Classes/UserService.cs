using AutoMapper;
using AutoMapper.QueryableExtensions;
using PWA.DAL.DTO.Identity;
using PWA.DAL.Models.Identity;
using PWA.DAL.Models.Enums;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ClaimTypes = PWA.DAL.Models.Identity.ClaimTypes;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;
using PWA.Services.Core.Implementation;

namespace PWA.Services.Classes
{
    using AutoMapper;
    using DAL.DTO;
    using DAL.Models;
    using Core.Implementation;
    using Interfaces;
    using DAL;
    public class UserReadService //: ReadServiceBase<User, UserDTO>, IUserReadService
    {
        private readonly UserManager<User> UserManager;
        private readonly SignInManager<User> SignInManager;
        private readonly RoleManager<Role> RoleManager;
        private readonly IMapper Mapper;

        public UserReadService(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IDataContext context,
            IMapper mapper,
            RoleManager<Role> roleManager)// : base(context, mapper)
        {
            UserManager = userManager;
            RoleManager = roleManager;
            SignInManager = signInManager;
            Mapper = mapper;
        }

        public async Task<UserDTO> GetAsync(Guid id)
        {
            try
            {
                var user = await UserManager.Users
                    .FirstOrDefaultAsync(u => u.Id == id);
                return await GetUserData(user);
            }
            catch
            {
                return null;
            }
        }
        public async Task<List<RoleDTO>> GetUserRoles(ClaimsPrincipal user)
        {
            try
            {
                var currentUser = await UserManager.GetUserAsync(user);
                var userRoles = await UserManager.GetRolesAsync(currentUser);
                return RoleManager.Roles.Where(r => userRoles.Contains(r.Name))
                    .ProjectTo<RoleDTO>(Mapper.ConfigurationProvider).ToList();
            }
            catch
            {
                return null;
            }
        }

        public async Task<UserDTO> GetUserByEmail(string email)
        {
            try
            {
                var user = await UserManager.FindByEmailAsync(email);
                return await GetUserData(user);
            }
            catch
            {
                return null;
            }
        }

        public async Task<UserDTO> GetLoggedUser(ClaimsPrincipal user)
        {
            var loggedUser = await UserManager.GetUserAsync(user);

            if (loggedUser == null) return null;

            var res = await GetUserData(loggedUser);
            res.Roles = res.Roles.Where(a => a.IsBelong).ToList();

            return res;
        }

        public async Task UserLogin(User user)
        {
            await SignInManager.SignInAsync(user, new AuthenticationProperties(), "RecoveryCode");
        }

        public async Task<bool> ResetPassword(ResetPasswordDTO resetPasswordDTO, CancellationToken cancellationToken,
            bool verify = true)
        {
            var user = await UserManager.FindByEmailAsync(resetPasswordDTO.Email);
            if (user == null)
            {
                return false;
            }

            if (user.LockoutEnabled && user.LockoutEnd > DateTime.UtcNow)
            {
                throw new Exception(
                    "Password can't be reset. User is locked out. Please, contact your system administrator.");
            }

            var code = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(resetPasswordDTO.Code));
            if (verify)
            {
                var result = await UserManager.ResetPasswordAsync(user, code, resetPasswordDTO.Password);

                if (!result.Succeeded)
                    throw new Exception(string.Join(Environment.NewLine,
                        result.Errors.Select(x => x.Description).ToList()));
            }

            //await PasswordChangedNotification(resetPasswordDTO.Email, cancellationToken);

            return true;
        }

        public async Task<bool> PasswordChangedNotification(string email, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(email)) return true;

            // var user = UserManager.FindByEmailAsync(email);
            // if (user == null)
            // {
            //     throw new Exception("The user is not found.");
            // }

            // var emailTemplate = await _context.Set<EmailTemplate>()
            //     .FirstOrDefaultAsync(t => t.Code == "PasswordChanged", cancellationToken);
            // if (emailTemplate == null)
            // {
            //     throw new Exception("Email Template error.");
            // }

            // var passwordChangedTemplate = Mapper.Map<EmailTemplateDTO>(emailTemplate);

            // var tagValues = new NameValueCollection
            // {
            //     {"$UserName", $"{user.Result.FirstName} {user.Result.LastName}"}
            // };

            // _emailTemplateService.BuildEmail(passwordChangedTemplate, tagValues);
            // _emailSender?.SendEmailAsync(passwordChangedTemplate.Subject, passwordChangedTemplate.Body, to: email);

            return true;
        }

        private async Task GetUserClaims(UserDTO userDto, User user)
        {
            var userClaimsFromDb = await UserManager.GetClaimsAsync(user);
            userDto.Claims = userClaimsFromDb
                .GroupBy(a => a.Type)
                .ToDictionary(k => k.Key, v =>
                {
                    var list = v.Select(a => a.Value).ToList();

                    return list.Count == 1 ? list[0] : JsonConvert.SerializeObject(list);
                });
        }

        private async Task<UserDTO> GetUserData(User user)
        {
            var userDto = Mapper.Map<UserDTO>(user);
            if (userDto == null) return null;

            var userRoles = await UserManager.GetRolesAsync(user);
            var roles = RoleManager.Roles;

            foreach (var role in roles)
            {
                userDto.Roles.Add(new UserRoleDTO
                {
                    Id = role.Id,
                    Name = role.Name,
                    IsBelong = userRoles.Contains(role.Name)
                });

                if (role.Name == RoleTypeEnum.SuperAdmin.ToString() && userRoles.Contains(role.Name))
                {
                    userDto.IsSuperAdmin = true;
                }
            }

            await GetUserClaims(userDto, user);
            return userDto;
        }
    }

    public class UserEditService //: EditServiceBase<User, UserDTO>, IUserEditService
    {
        private readonly UserManager<User> UserManager;
        private readonly SignInManager<User> SignInManager;
        private readonly RoleManager<Role> RoleManager;
        private readonly IMapper Mapper;

        public UserEditService(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IDataContext context,
            IMapper mapper,
            RoleManager<Role> roleManager) //: base(context, mapper)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;
            Mapper = mapper;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var user = await UserManager.Users.FirstOrDefaultAsync(u => u.Id == id);
            var res = await UserManager.DeleteAsync(user);
            return res.Succeeded;
        }

        public async Task<Guid> SaveAsync(UserDTO item)
        {
            try
            {
                User user;

                if (item.Id == null)
                {
                    // Create a new user
                    user = Mapper.Map<User>(item);
                    await UserManager.CreateAsync(user, item.Password);
                }
                else
                {
                    // Update a current user
                    user = await UserManager.Users.FirstOrDefaultAsync(u => u.Id == item.Id);
                    user.FirstName = item.FirstName;
                    user.LastName = item.LastName;
                    user.Email = item.Email;
                    user.PhoneNumber = item.PhoneNumber;
                    user.UserName = item.Email;
                    user.NormalizedEmail = item.Email.ToUpper();
                    user.NormalizedUserName = item.Email.ToUpper();
                   // user.RecoveryCode = item.RecoveryCode;
                    await UserManager.UpdateAsync(user);
                }

                // update user roles and claims
                await UpdateUserRoles(user, item.Roles);
                await UpdateUserClaims(user, item.Claims);

                return user.Id;
            }
            catch
            {
                return Guid.Empty;
            }
        }

        private async Task UpdateUserRoles(User user, IEnumerable<UserRoleDTO> newRoles)
        {
            var userRolesFromDb = await UserManager.GetRolesAsync(user);

            foreach (var role in newRoles)
            {
                if (!userRolesFromDb.Contains(role.Name) && role.IsBelong)
                {
                    await UserManager.AddToRoleAsync(user, role.Name);
                }

                if (userRolesFromDb.Contains(role.Name) && !role.IsBelong)
                {
                    await UserManager.RemoveFromRoleAsync(user, role.Name);
                }
            }
        }

        private async Task UpdateUserClaims(User user, Dictionary<string, string> claims)
        {
            var userClaimsFromDb = await UserManager.GetClaimsAsync(user);

            foreach (var claim in claims)
            {
                var claimDb = userClaimsFromDb.FirstOrDefault(c =>
                    string.Equals(c.Type, claim.Key, StringComparison.CurrentCultureIgnoreCase));

                if (claimDb != null)
                {
                    if (claimDb.Value != claim.Value)
                    {
                        await UserManager.ReplaceClaimAsync(user, claimDb, new Claim(claim.Key, claim.Value));
                    }
                }
                else
                {
                    await UserManager.AddClaimAsync(user, new Claim(claim.Key, claim.Value));
                }
            }
        }

        public async Task<IdentityResult> Register(UserRegistrationDTO userRegistrationData)
        {
            var user = new User
            {
                UserName = userRegistrationData.Email,
                FirstName = userRegistrationData.FirstName,
                LastName = userRegistrationData.LastName,
                Email = userRegistrationData.Email
            };

            var result = await UserManager.CreateAsync(user, userRegistrationData.Password);

            await UserManager.AddToRoleAsync(user, RoleTypeEnum.User.ToString());
            await UpdateStatus(user.Id, AccountStatusEnum.Unverified);
            return result;
        }


        public async Task<int> CheckPwnedPasswordAsync(UserRegistrationDTO userRegistrationData)
        {
            try
            {
                var urlPassword = "https://api.pwnedpasswords.com/range/";
                var client = new HttpClient { BaseAddress = new Uri(urlPassword) };
                var SHA1 = userRegistrationData.PasswordSHA1.Substring(0, 5);
                var search = userRegistrationData.PasswordSHA1.Replace(SHA1, "");
                var response = await client.GetAsync(SHA1);
                var nbr = 0;
                if (!response.IsSuccessStatusCode) return nbr;

                var res = await response.Content.ReadAsStringAsync();
                if (string.IsNullOrEmpty(res)) return nbr;

                var result = res.Split("\r\n").ToList();
                foreach (var str in result)
                {
                    var tab = str.Split(":");
                    if (string.Equals(tab[0], search, StringComparison.CurrentCultureIgnoreCase))
                    {
                        nbr = Convert.ToInt32(tab[1]);
                    }
                }

                return nbr;
            }
            catch
            {
                return 0;
            }
        }

        public async Task UserLogin(User user)
        {
            await SignInManager.SignInAsync(user,  new AuthenticationProperties(), "RecoveryCode");
        }

        public async Task UserLogout()
        {
            await SignInManager.SignOutAsync();
        }

        public async Task<NameValueCollection> GetTagValuesAsync(string email, string url, string ipAddress = "")
        {
            var user = await UserManager.FindByEmailAsync(email);
            if (user == null)
            {
                return null;
            }

            var code = await GeneratePasswordResetToken(user);
            var callbackUrl = $"{url}?userId={user.Id}&code={code}";
            var tagValues = new NameValueCollection
                {
                    {"$UserName", $"{user.FirstName} {user.LastName}"},
                    {"$UserLink", callbackUrl},
                    {"$IpAddress", ipAddress},
                };
            return tagValues;

        }

        public async Task<RecoverPasswordData> RecoverPassword(RecoverPasswordDTO recoverPassword, string url,
            string ip)
        {
            var recoverPasswordData = new RecoverPasswordData
            {
                CreatedDate = DateTime.Now
            };
            return recoverPasswordData;
            // try
            // {
            //     if (recoverPassword == null)
            //     {
            //         recoverPasswordData.ErrorMessage = "The email cannot be sent. Email address is undefined.";
            //         return recoverPasswordData;
            //     }


            //     var emailTemplate =
            //         await _context.Set<EmailTemplate>().FirstOrDefaultAsync(t => t.Code == "ResetPassword");
            //     if (emailTemplate == null)
            //     {
            //         recoverPasswordData.ErrorMessage = "Email Template error.";
            //         return recoverPasswordData;
            //     }

            //     var resetPasswordTemplate = Mapper.Map<EmailTemplateDTO>(emailTemplate);

            //     var tagValues = await GetTagValuesAsync(recoverPassword.Email, url, ip);
            //     if (tagValues == null)
            //     {
            //         recoverPasswordData.ErrorMessage = "user not found";
            //         return recoverPasswordData;
            //     }

            //     _emailTemplateService.BuildEmail(resetPasswordTemplate, tagValues);
            //     await _emailSender.SendEmailAsync(resetPasswordTemplate.Subject, resetPasswordTemplate.Body,
            //         recoverPassword.Email);

            //     recoverPasswordData.Link = tagValues["$UserLink"];

            //     return recoverPasswordData;
            // }
            // catch (Exception ex)
            // {
            //     Log.Error(ex, ex.Message);
            //     recoverPasswordData.ErrorMessage = ex.Message;
            //     return recoverPasswordData;
            // }
        }

        public async Task<bool> SendInvite(UserDTO userData, string scheme, string host, bool genToken)
        {
            // if (userData.Id == null)
            // {
            //     // Create a new user
            //     var user = new User
            //     {
            //         UserName = userData.Email,
            //         Email = userData.Email,
            //         FirstName = userData.FirstName,
            //         LastName = userData.LastName,
            //         CompanyId = userData.CompanyId,
            //         InvitationEnd = DateTime.Now.AddDays(30),
            //     };

            //     var result = await UserManager.CreateAsync(user);
            //     if (!result.Succeeded)
            //     {
            //         throw new Exception("User create error");
            //     }
            // }

            // // User roles update
            // var userUpdate = UserManager.Users.First(x => x.Email == userData.Email);
            // if (userUpdate == null) throw new Exception("Unknown user error");

            // userUpdate.InvitationEnd = DateTime.Now.AddDays(30);
            // await UpdateUserRoles(userUpdate, userData.Roles);
            // await UpdateStatus(userUpdate.Id, AccountStatusEnum.Invited);

            // // Generate callbackUrl
            // var code = genToken ? await GeneratePasswordResetToken(userUpdate) : Guid.NewGuid().ToString();
            // var callbackUrl = $"{scheme}://{host}/account/activate?userId={userUpdate.Id}&code={code}";
            // var emailTo = userUpdate.Email;

            // // Send email
            // var emailTemplate =
            //     await _context.Set<EmailTemplate>().FirstOrDefaultAsync(t => t.Code == "UserInvitation");
            // if (emailTemplate == null)
            // {
            //     throw new Exception("Email Template error.");
            // }

            // var userInvitationTemplate = Mapper.Map<EmailTemplateDTO>(emailTemplate);

            // var tagValues = new NameValueCollection
            // {
            //     {"$UserName", $"{userUpdate.FirstName} {userUpdate.LastName}"},
            //     {"$CallbackUrl", callbackUrl}
            // };

            // _emailTemplateService.BuildEmail(userInvitationTemplate, tagValues);
            // await _emailSender.SendEmailAsync(userInvitationTemplate.Subject, userInvitationTemplate.Body, to: emailTo);

            return true;
        }

        private async Task<string> GeneratePasswordResetToken(User user)
        {
            var token = await UserManager.GeneratePasswordResetTokenAsync(user);
            return WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(token));
        }

        public async Task LockUnlock(UserDTO item, CancellationToken cancellationToken)
        {
            var user = await UserManager.Users.FirstOrDefaultAsync(u => u.Id == item.Id);
            await UserManager.SetLockoutEnabledAsync(user, !user.LockoutEnabled);
            if (!user.LockoutEnabled) await UpdateStatus(user.Id, AccountStatusEnum.Suspended);
            else await UpdateStatus(user.Id, AccountStatusEnum.Active);
        }

        public async Task<bool> UpdateStatus(Guid id, AccountStatusEnum status)
        {
            var user = await UserManager.Users.FirstOrDefaultAsync(u => u.Id == id);
           // user.AccountStatus = status;
            await UserManager.UpdateAsync(user);
            return true;
        }

        public async Task<bool> ResendEmailConfirmation(UserDTO userData, string scheme, string host,
            bool genToken = true)
        {
            // // User roles update
            // var userUpdate = UserManager.Users.FirstOrDefault(x => x.Id == userData.Id);
            // if (userUpdate == null) throw new Exception("Unknown user error");

            // // Generate callbackUrl
            // var code = genToken ? await GenerateEmailConfirmationTokenAsync(userUpdate) : Guid.NewGuid().ToString();
            // var callbackUrl = $"{scheme}://{host}/account/confirmationemail?userId={userUpdate.Id}&code={code}";
            // var emailTo = userData.Email;

            // // Send email
            // var emailTemplate =
            //     await _context.Set<EmailTemplate>().FirstOrDefaultAsync(t => t.Code == "EmailConfirmation");
            // if (emailTemplate == null)
            // {
            //     throw new Exception("Email Template error.");
            // }

            // var userInvitationTemplate = Mapper.Map<EmailTemplateDTO>(emailTemplate);

            // var tagValues = new NameValueCollection
            // {
            //     {"$UserName", $"{userUpdate.FirstName} {userUpdate.LastName}"},
            //     {"$CallbackUrl", callbackUrl}
            // };

            // userUpdate.EmailConfirmed = false;
            // await UserManager.UpdateAsync(userUpdate);

            // _emailTemplateService.BuildEmail(userInvitationTemplate, tagValues);
            // await _emailSender.SendEmailAsync(userInvitationTemplate.Subject, userInvitationTemplate.Body, to: emailTo);

            return true;
        }


        public async Task<bool> SendEmailChangeEmail(User userData, string scheme, string host,
            bool genToken = true)
        {
            // var emailTemplate =
            //     await _context.Set<EmailTemplate>().FirstOrDefaultAsync(t => t.Code == "ChangeEmail_v3");
            // if (emailTemplate == null)
            // {
            //     throw new Exception("Email Template error.");
            // }

            // var resetPasswordTemplate = Mapper.Map<EmailTemplateDTO>(emailTemplate);

            // var tagValues = new NameValueCollection
            // {
            //     {"$UserName", $"{userData.FirstName} {userData.LastName}"}
            // };

            // _emailTemplateService.BuildEmail(resetPasswordTemplate, tagValues);
            // await _emailSender.SendEmailAsync(resetPasswordTemplate.Subject, resetPasswordTemplate.Body,
            //     to: userData.Email);
            return true;
        }

        private async Task<string> GenerateEmailConfirmationTokenAsync(User user)
        {
            var token = await UserManager.GenerateEmailConfirmationTokenAsync(user);
            return WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(token));
        }

        public async Task<bool> SendNotificationNewEmail(UserDTO userUpdate, HttpRequest request,
            CancellationToken cancellationToken)
        {
            if (userUpdate == null) return true;

            var user = await UserManager.Users.FirstOrDefaultAsync(u => u.Id == userUpdate.Id);
            if (user == null)
            {
                throw new Exception("The user is not found.");
            }

            var oldEmail = user.Email;
            if (oldEmail != userUpdate.Email)
            {
                await ResendEmailConfirmation(userUpdate, request.Scheme, request.Host.ToString());
                await SendEmailChangeEmail(user, request.Scheme, request.Host.ToString());
            }

            return true;
        }
    }

    public class UserService : CRUDServiceBase<UserDTO, UserDTO>, IUserService
    {
        private IUserEditService EditService => (IUserEditService)EditService;
        private IUserReadService ReadService => (IUserReadService)ReadService;

        public UserService(IUserReadService readService, IUserEditService editService) : base(readService, editService)
        {
        }

        public Task<IdentityResult> Register(UserRegistrationDTO userRegistrationData)
        {
            return EditService.Register(userRegistrationData);
        }

        public Task UserLogin(User user)
        {
            return EditService.UserLogin(user);
        }

        public Task<bool> SendInvite(UserDTO userData, string scheme, string host, bool genToken = true)
        {
            return EditService.SendInvite(userData, scheme, host, genToken);
        }

        public Task<bool> ResendEmailConfirmation(UserDTO userData, string scheme, string host, bool genToken = true)
        {
            return EditService.ResendEmailConfirmation(userData, scheme, host, genToken);
        }

        public Task UserLogout()
        {
            return EditService.UserLogout();
        }

        public Task<UserDTO> GetUserByEmail(string email)
        {
            return ReadService.GetUserByEmail(email);
        }

        public Task<UserDTO> GetUserById(Guid id, CancellationToken token)
        {
            return ReadService.GetAsync(id, token);
        }

        public async Task<UserDTO> GetLoggedUser(ClaimsPrincipal user)
        {
            return await ReadService.GetLoggedUser(user);
        }

        public Task<bool> ResetPassword(ResetPasswordDTO email, CancellationToken cancellationToken, bool verify)
        {
            return ReadService.ResetPassword(email, cancellationToken, verify);
        }

        public Task<bool> SendNotificationNewEmail(UserDTO userUpdate, HttpRequest request,
            CancellationToken cancellationToken)
        {
            return EditService.SendNotificationNewEmail(userUpdate, request, cancellationToken);
        }

        public async Task<NameValueCollection> GetTagValuesAsync(string email, string url, string ipAddress = "")
        {
            return await EditService.GetTagValuesAsync(email, url, ipAddress);
        }

        public Task LockUnlock(UserDTO item, CancellationToken cancellationToken) =>
            EditService.LockUnlock(item, cancellationToken);

        public async Task<int> CheckPwnedPasswordAsync(UserRegistrationDTO userRegistrationData)
        {
            return await EditService.CheckPwnedPasswordAsync(userRegistrationData);
        }

        public async Task<bool> UpdateStatus(Guid id, AccountStatusEnum statusName)
        {
            return await EditService.UpdateStatus(id, statusName);
        }
    }

    public class RecoverPasswordData
    {
        public string Link { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}