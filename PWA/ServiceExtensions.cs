﻿namespace PWA
{
    using System;
    using System.Text;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.IdentityModel.Tokens;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using PWA.DAL;
    using PWA.DAL.Models.Identity;
    using PWA.Services.Classes;
    using PWA.Services.Interfaces;
    using PWA.DAL.Config;
    using Microsoft.Extensions.Configuration;

    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("EnableCORS", builder =>
                {
                    builder
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                        .Build();
                });
            });
        }

        public static void ConfigureAuthentication(this IServiceCollection services, IConfiguration configuration, string jwtSection = "Jwt")
        {
            services.Configure<JwtConfig>(configuration.GetSection(jwtSection));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidIssuer = configuration[$"{jwtSection}:Issuer"],
                        ValidAudience = configuration[$"{jwtSection}:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration[$"{jwtSection}:Key"]))
                    };
                });
        }

        public static IServiceCollection AddSqlServerSignInManager<TContext>(this IServiceCollection services, Action<IdentityOptions> identityOptions) where TContext : DbContext
        {
            services
                .AddIdentity<User, Role>(identityOptions)
                .AddEntityFrameworkStores<TContext>()
                .AddDefaultTokenProviders()
                .AddUserStore<Microsoft.AspNetCore.Identity.EntityFrameworkCore.UserStore<User, Role, TContext, Guid, IdentityUserClaim<Guid>, UserRole, IdentityUserLogin<Guid>, IdentityUserToken<Guid>, IdentityRoleClaim<Guid>>>()
                .AddRoleStore<Microsoft.AspNetCore.Identity.EntityFrameworkCore.RoleStore<Role, TContext, Guid, UserRole, IdentityRoleClaim<Guid>>>()
                .AddSignInManager<SignInManager<User>>();

            return services;
        }

        public static void ConfigureDI(this IServiceCollection services)
        {
            services.AddScoped<IDataContext, Context>();
            
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ICategoryReadService, CategoryReadService>();
            services.AddScoped<ICategoryEditService, CategoryEditService>();
        }

        public static void RegisterMembershipServices(this IServiceCollection services)
        {
            // Users
            // services.AddScoped<IUserReadService, UserReadService>();
            // services.AddScoped<IUserEditService, UserEditService>();
            // services.AddScoped<IUserService, UserService>();
        }
    }
}
