namespace PWA.DAL.Models.Identity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Microsoft.AspNetCore.Identity;
    using PWA.DAL.Core;
    using PWA.DAL.Models.Enums;

    /// <summary>
    /// User class definition
    /// </summary>
    public class User : IdentityUser<Guid>
    {
        [StringLength(35)]
        public string FirstName { get; set; }

        [StringLength(35)]
        public string LastName { get; set; } 

        [StringLength(20)]
        public override string PhoneNumber { get; set; }
        public AccountStatusEnum AccountStatus { get; set; }
        public string EmailConfirmationToken { get; set; }

        //public string RecoveryCode { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }

        public virtual ICollection<UserGroup> UserGroups { get; set; }
        //public AccountStatusEnum AccountStatus { get; set; }
    }
}