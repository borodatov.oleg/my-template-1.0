﻿namespace PWA.Services.Interfaces
{
    using Core;
    using DAL.DTO;

    public interface ICategoryReadService : IReadService<CategoryDto>
    {
    }

    public interface ICategoryEditService : IEditService<CategoryDto>
    {
    }

    public interface ICategoryService : ICategoryReadService, ICategoryEditService
    {
    }
}
