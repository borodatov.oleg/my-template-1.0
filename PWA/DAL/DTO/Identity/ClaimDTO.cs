﻿namespace PWA.DAL.DTO.Identity
{
    public class ClaimDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public bool IsBelong { get; set; }
    }
}
