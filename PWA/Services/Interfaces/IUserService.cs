using PWA.DAL.DTO.Identity;
using PWA.DAL.Models.Identity;
using PWA.DAL.Models.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace PWA.Services.Interfaces
{
    using Core;
    using DAL.DTO;
    public interface IUserReadService : IReadService<UserDTO>
    {
        Task<UserDTO> GetUserByEmail(string email);
        Task<UserDTO> GetLoggedUser(ClaimsPrincipal user);
        Task<bool> ResetPassword(ResetPasswordDTO email, CancellationToken cancellationToken, bool verify = true);
    }

    public interface IUserEditService : IEditService<UserDTO>
    {
        Task<IdentityResult> Register(UserRegistrationDTO userRegistrationData);
        Task UserLogin(User user);
        Task<bool> SendInvite(UserDTO userData, string scheme, string host, bool genToken = true);
        Task UserLogout();
        Task<NameValueCollection> GetTagValuesAsync(string email, string url, string ipAdress);
        Task LockUnlock(UserDTO item, CancellationToken cancellationToken);
        Task<int> CheckPwnedPasswordAsync(UserRegistrationDTO userRegistrationData);
        Task<bool> UpdateStatus(Guid id, AccountStatusEnum status);
        Task<bool> ResendEmailConfirmation(UserDTO userData, string scheme, string host, bool genToken = true);
        Task<bool> SendNotificationNewEmail(UserDTO userUpdate, HttpRequest request, CancellationToken cancellationToken);
    }

    public interface IUserService : IUserReadService, IUserEditService
    {

    }
}
