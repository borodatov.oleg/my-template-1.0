namespace PWA.DAL.DTO.Identity
{
	public class RecoveryCodeDTO
	{
		public string UserId { get; set; }
		public string Code { get; set; }
	}
}