using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PWA.DAL.Models.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AccountStatusEnum
    {
        [EnumMember(Value = "Invited")] Invited = 2,
        [EnumMember(Value = "Unapproved")] Unapproved = 3,
        [EnumMember(Value = "Unverified")] Unverified = 4,
        [EnumMember(Value = "Active")] Active = 5,
        [EnumMember(Value = "Suspended")] Suspended = 6,
        [EnumMember(Value = "Deleted")] Deleted = 7
    }
}