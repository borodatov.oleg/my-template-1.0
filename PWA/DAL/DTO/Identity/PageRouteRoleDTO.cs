using System.Collections.Generic;

namespace PWA.DAL.DTO.Identity
{
    public class PageRouteRoleDTO
    {
        public IEnumerable<PageRolesDTO> PageRoles { get; set; }
        public IEnumerable<RouteRolesDTO> RouteRoles { get; set; }
        public int total { get; set; }
    }
}