﻿namespace PWA.DAL.Core
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Base class for all entities
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// Entity identity field
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid UserId { get; set; }
    }
}
