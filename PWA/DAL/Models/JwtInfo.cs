namespace PWA.DAL.Models
{
    public class JwtInfo
    {
        public string Token { get; set; }

        public string Audience { get; set; }
    }
}